# Microservices Project

## Description
A microservices project for CRUD operations with MongoDB, JWT authentication, Redis caching, and Kafka integration.

## Setup

### Prerequisites
- Node.js
- Docker

### Installation

1. Clone the repository:
   ```bash
   git clone <repository-url>
   cd project-root
