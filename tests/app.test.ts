import { Request, Response } from 'express';
import * as accountController from '../src/controllers/accountController';

describe('Account Controller', () => {
    let mockRequest: Partial<Request>;
    let mockResponse: Partial<Response>;

    beforeEach(() => {
        mockRequest = {};
        mockResponse = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
            sendStatus: jest.fn()
        };
    });

    it('should create a new account', async () => {
        mockRequest.body = {
            accountId: '123',
            userName: 'testuser',
            password: 'password123',
            userId: '456'
        };

        await accountController.createAccount(mockRequest as Request, mockResponse as Response);

        expect(mockResponse.status).toHaveBeenCalledWith(201);
        expect(mockResponse.json).toHaveBeenCalled();
    });

    it('should handle error when account creation fails', async () => {
        // Simulate error case (e.g., missing required fields)
        mockRequest.body = {
            userName: 'testuser',
            password: 'password123'
            // Missing accountId and userId intentionally to trigger an error
        };

        await accountController.createAccount(mockRequest as Request, mockResponse as Response);

        expect(mockResponse.sendStatus).toHaveBeenCalledWith(500);
        expect(mockResponse.json).toHaveBeenCalledWith({ error: expect.any(String) });
    });
});
