import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import routes from './routes/routes';
import {authenticateToken} from './middlewares/auth';
import {connectRedis} from './utils/redis';
import {KAFKA_URI, MONGODB_URI} from './config';
import kafka from 'kafka-node';

// Initialize Kafka consumer
const Consumer = kafka.Consumer;
const client = new kafka.KafkaClient({ kafkaHost: KAFKA_URI});
const consumer = new Consumer(
    client,
    [{ topic: 'kafka_bagas_betest', partition: 0 }],
    { autoCommit: true }
);

// Event handler for incoming messages
consumer.on('message', async (message) => {
    try {
        // Process Kafka message
        console.log('Received message:', message.value.toString());
        // Your processing logic here
    } catch (error) {
        console.error('Error processing Kafka message:', error);
    }
});



const app = express();
const PORT = 3000;

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
// app.use(authenticateToken);

// Routes
app.use('/api', routes);

// Connect to MongoDB
mongoose.connect(MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log('Connected to MongoDB');
    })
    .catch((error) => {
        console.error('MongoDB connection error:', error);
    });


// Connect to Redis
connectRedis();

// Start server
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});
