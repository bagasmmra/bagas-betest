declare module 'kafka-node' {
    export class KafkaClient {
        constructor(options: { kafkaHost: string });
    }

    export class Consumer {
        constructor(client: KafkaClient, payloads: Array<{ topic: string; partition: number }>, options: { autoCommit: boolean });
        on(event: 'message', callback: (message: { value: string }) => void): void;
    }
}
