import { Request, Response } from 'express';
import { JwtService } from '../services/jwtService';
import bcrypt from 'bcryptjs';
import UserInfo from "../models/UserInfo";
import { v4 as uuidv4 } from 'uuid';
import {setUserInfoCache} from "../utils/redis";

export class AuthController {
    async generateJwtToken (req: Request, res: Response): Promise<void> {
        try {
            const {userId} = req.body;
            const token = JwtService.generateToken({userId});
            res.status(200).json({token});
        } catch (error: any) {
            res.status(500).json({error: error.message});
        }
    }

    async authenticateUser (req: Request, res: Response): Promise<void> {
        try {
            const {userName, password} = req.body;
            const userInfo = await UserInfo.findOne({ emailAddress : userName }).exec();
            const mockUserId = userInfo?.userId;
            const mockHashedPassword = await bcrypt.hash('password123', 10); // Replace with actual hashed password

            // Compare hashed password (simulated)
            const passwordMatch = await bcrypt.compare(password, mockHashedPassword);

            if (userName === userInfo?.emailAddress && passwordMatch) {

                const accountKey = `account:${mockUserId}`;
                const accountId = uuidv4();

                const accountData = {
                    userName,
                    password,
                    userId : mockUserId,
                    accountId
                };

                setUserInfoCache(accountKey, accountData);
                const token = JwtService.generateToken({userId: mockUserId});
                res.status(200).json({token});
            } else {
                res.status(401).json({message: 'Invalid credentials'});
            }
        } catch (error: any) {
            res.status(500).json({error: error.message});
        }
    }

}
