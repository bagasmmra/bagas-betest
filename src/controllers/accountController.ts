import { Request, Response } from 'express';
import AccountLogin, { AccountLoginDocument } from '../models/AccountLogin';
import { setUserInfoCache } from '../utils/redis';
import { v4 as uuidv4 } from 'uuid';

export class AccountController {
    async createAccount (req: Request, res: Response): Promise<void> {
        try {
            const {accountId, userName, password, userId} = req.body;

            const accountKey = `account:${accountId}`;
            const transactionId = uuidv4();

            const accountData = {
                accountId,
                userName,
                password,
                userId,
                transactionId
            };

            setUserInfoCache(accountKey, accountData);


            // res.status(201).json(accountData);
        } catch (error: any) {
            res.status(500).json({error: error.message});
        }
    }

    async getAccountLoginsAfterThreeDays (req: Request, res: Response): Promise<void> {
        try {
            const threeDaysAgo = new Date(Date.now() - 3 * 24 * 60 * 60 * 1000);
            const accountLogins = await AccountLogin.find({lastLoginDateTime: {$gt: threeDaysAgo}}).exec();
            res.status(200).json(accountLogins);
        } catch (error: any) {
            res.status(500).json({error: error.message});
        }
    }
}
