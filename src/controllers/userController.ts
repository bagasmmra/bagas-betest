import { Request, Response } from 'express';
import UserInfo from '../models/UserInfo';
import { v4 as uuidv4 } from 'uuid';

export class UserController {
    async getUserInfoByAccountNumber(req: Request, res: Response): Promise<void> {
        try {
            const accountNumber = req.params.accountNumber;
            const userInfo = await UserInfo.findOne({ accountNumber }).exec();
            if (!userInfo) {
                res.status(404).json({ message: 'User not found' });
                return;
            }
            res.status(200).json(userInfo);
        } catch (error:any) {
            res.status(500).json({ error: error.message });
        }
    }

    async getUserInfoByRegistrationNumber(req: Request, res: Response): Promise<void> {
        try {
            const registrationNumber = req.params.registrationNumber;
            const userInfo = await UserInfo.findOne({ registrationNumber }).exec();
            if (!userInfo) {
                res.status(404).json({ message: 'User not found' });
                return;
            }
            res.status(200).json(userInfo);
        } catch (error:any) {
            res.status(500).json({ error: error.message });
        }
    }

    async createUser(req: Request, res: Response): Promise<void> {
        try {
            const {fullName, accountNumber, emailAddress, registrationNumber} = req.body;

            const userInfo = await UserInfo.findOne({ emailAddress }).exec();
            if (userInfo) {
                res.status(404).json({ message: 'User already exists' });
                return;
            }

            const userId = uuidv4();

            const data = {
                fullName,
                accountNumber,
                emailAddress,
                userId,
                registrationNumber
            };

            await UserInfo.create(data);

            res.status(200).json(data);
        } catch (error:any) {
            res.status(500).json({ error: error.message });
        }
    }
}
