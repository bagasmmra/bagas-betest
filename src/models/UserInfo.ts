import mongoose, { Schema, Document } from 'mongoose';

export interface UserInfoDocument extends Document {
    userId: string;
    fullName: string;
    accountNumber: string;
    emailAddress: string;
    registrationNumber: string;
}

const UserInfoSchema: Schema = new Schema({
    userId: { type: String, required: true },
    fullName: { type: String, required: true },
    accountNumber: { type: String, required: true },
    emailAddress: { type: String, required: true },
    registrationNumber: { type: String, required: true }
});

export default mongoose.model<UserInfoDocument>('UserInfo', UserInfoSchema);
