import mongoose, { Schema, Document } from 'mongoose';

export interface AccountLoginDocument extends Document {
    accountId: string;
    userName: string;
    password: string;
    lastLoginDateTime: Date;
    userId: string;
}

const AccountLoginSchema: Schema = new Schema({
    accountId: { type: String, required: true },
    userName: { type: String, required: true },
    password: { type: String, required: true },
    lastLoginDateTime: { type: Date, default: Date.now },
    userId: { type: String, required: true }
});

export default mongoose.model<AccountLoginDocument>('AccountLogin', AccountLoginSchema);
