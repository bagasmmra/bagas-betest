import Redis from 'ioredis';
import { REDIS_URI } from '../config';
import { v4 as uuidv4 } from 'uuid';

let redisClient: Redis | undefined;

export const connectRedis = (): void => {
    redisClient = new Redis(REDIS_URI);
    redisClient.on('error', (err) => {
        console.error('Redis connection error:', err);
    });
    redisClient.on('connect', () => {
        console.log('Connected to Redis');
    });
};

export const setUserInfoCache = async (key: string, value: object): Promise<void> => {
    if (!redisClient) {
        throw new Error('Redis client is not connected.');
    }

    const transactionId = uuidv4();
    const accountData = { ...value, transactionId };

    try {
        const result = await (redisClient as any).set(
            key,
            JSON.stringify(accountData),
            'EX', 3600, 'NX'
        );


        if (result === 'OK') {
            console.log(`Cache set successfully for key: ${key}`);
        } else {
            console.log(`Key: ${key} already exists in Redis, skipping set operation.`);
        }
    } catch (error) {
        console.error(`Error setting cache for key: ${key}`, error);
        throw new Error(`Error setting cache for key: ${key}`);
    }
};

export const getUserInfoCache = async (key: string): Promise<object | null> => {
    if (!redisClient) {
        throw new Error('Redis client is not connected.');
    }
    try {
        const cachedData = await redisClient.get(key);
        return cachedData ? JSON.parse(cachedData) : null;
    } catch (error) {
        console.error(`Error getting cache for key: ${key}`, error);
        throw new Error(`Error getting cache for key: ${key}`);
    }
};
