import { Router } from 'express';
import { UserController } from '../controllers/userController';
import { AccountController } from '../controllers/accountController';
import { AuthController } from '../controllers/authController';
import { authenticateToken } from '../middlewares/auth';

const router = Router();
const userController = new UserController();
const accountController = new AccountController();
const authController = new AuthController();

// User Info routes
router.get('/users/account/:accountNumber', authenticateToken, userController.getUserInfoByAccountNumber);
router.get('/users/registration/:registrationNumber', authenticateToken, userController.getUserInfoByRegistrationNumber);
router.post('/users', userController.createUser);

// Account Login routes
router.post('/accounts/create', accountController.createAccount);
router.get('/accounts/lastLoginAfterThreeDays', authenticateToken, accountController.getAccountLoginsAfterThreeDays);

// Auth routes
router.post('/auth/token', authController.generateJwtToken);
router.post('/auth/login', authController.authenticateUser);

export default router;
