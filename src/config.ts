export const JWT_SECRET = process.env.JWT_SECRET || 'd31cdfaea29e4ab6a7ef5ea32c22caa1db864ea0669a1efa9a73d649b3a1c6e6';

// Other configurations can be added here, like database URIs, Kafka settings, etc.
export const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://localhost:27017/db_bagas_betest';
export const REDIS_URI = process.env.REDIS_URI || 'redis://localhost:6379';
export const KAFKA_URI = process.env.KAFKA_URI || 'kafka:9092';