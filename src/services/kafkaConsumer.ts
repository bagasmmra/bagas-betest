import kafka from 'kafka-node';
import UserInfo from '../models/UserInfo';
import { KAFKA_URI } from "../config";
import { getUserInfoCache } from '../utils/redis';
import AccountLogin from "../models/AccountLogin"; // Adjust the path accordingly

const Consumer = kafka.Consumer;
const client = new kafka.KafkaClient({ kafkaHost: KAFKA_URI });
const consumer = new Consumer(
    client,
    [{ topic: 'kafka_bagas_betest', partition: 0 }],
    { autoCommit: true }
);

consumer.on('message', async (message) => {
    try {
        // Assuming the message value is a JSON string
        const messageValue = JSON.parse(message.value.toString());
        console.log('Received message:', messageValue);

        // Adjust the key extraction based on your actual message structure
        const key = messageValue.userId;  // Example: Assuming userId is the identifier

        if (!key) {
            console.error('No key found in message value:', messageValue);
            return;
        }

        const userData = await getUserInfoCache(key);

        if (!userData) {
            console.error(`No user data found in cache for key: ${key}`);
            return;
        }

        const user = new AccountLogin(userData);

        await user.save();
        console.log(`User data saved successfully for key: ${key}`);
    } catch (error) {
        console.error('Error processing Kafka message:', error);
    }
});
