import jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../config';

export class JwtService {
    static generateToken(payload: any): string {
        return jwt.sign(payload, JWT_SECRET, { expiresIn: '1h' });
    }

    static verifyToken(token: string): any {
        return jwt.verify(token, JWT_SECRET);
    }
}
